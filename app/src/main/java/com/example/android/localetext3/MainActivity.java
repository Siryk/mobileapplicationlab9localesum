package com.example.android.localetext3;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private int inputQuantity = 1;
    private final NumberFormat numberFormat = NumberFormat.getInstance();
    private double price = 0.10;
    private final double frExchangeRate = 0.93;
    private final double iwExchangeRate = 3.61;
    private NumberFormat currencyFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> showHelp());

        final Date currentDate = new Date();
        final long expirationDate = currentDate.getTime() + TimeUnit.DAYS.toMillis(5);
        currentDate.setTime(expirationDate);

        String formattedDate = DateFormat.getDateInstance().format(currentDate);
        TextView expirationDateView = findViewById(R.id.date);
        expirationDateView.setText(formattedDate);

        String formattedPrice;
        String deviceLocale = Locale.getDefault().getCountry();
        if (deviceLocale.equals("FR") || deviceLocale.equals("IL")) {
            if (deviceLocale.equals("FR")) {
                price *= frExchangeRate;
            } else {
                price *= iwExchangeRate;
            }
            currencyFormat = NumberFormat.getCurrencyInstance();
        } else {
            currencyFormat = NumberFormat.getCurrencyInstance(Locale.US);
        }
        formattedPrice = currencyFormat.format(price);

        TextView localePrice = findViewById(R.id.price);
        localePrice.setText(formattedPrice);

        final EditText enteredQuantity = findViewById(R.id.quantity);

        enteredQuantity.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                try {
                    inputQuantity = numberFormat.parse(v.getText().toString()).intValue();
                    v.setError(null);
                } catch (ParseException e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                    v.setError(getText(R.string.enter_number));
                    return false;
                }

                String formattedQuantity = numberFormat.format(inputQuantity);
                v.setText(formattedQuantity);

                double totalAmount = price * inputQuantity;

                if (Locale.getDefault().getCountry().equals("FR")) {
                    currencyFormat = NumberFormat.getCurrencyInstance(Locale.FRANCE);
                } else if (Locale.getDefault().getCountry().equals("IL")) {
                    currencyFormat = NumberFormat.getCurrencyInstance(new Locale("iw", "IL"));
                } else {
                    currencyFormat = NumberFormat.getCurrencyInstance(Locale.US);
                }

                String formattedTotalAmount = currencyFormat.format(totalAmount);
                TextView totalAmountView = findViewById(R.id.total);
                totalAmountView.setText(formattedTotalAmount);

                return true;
            }
            return false;
        });
    }

    private void showHelp() {
        Intent helpIntent = new Intent(this, HelpActivity.class);
        startActivity(helpIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((EditText) findViewById(R.id.quantity)).getText().clear();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_help:
                showHelp();
                return true;
            case R.id.action_language:
                Intent languageIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
                startActivity(languageIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
